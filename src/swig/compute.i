%module compute

%{
#include "compute.h"
#include "point.h"
#include "tcl_point.h"
%}

%include "compute.h"
%include "point.i"
