#include <tcl.h>

extern "C"
{
    extern int Compute_Init(Tcl_Interp *interp);
}

// Tcl_AppInitProc Test_AppInit;

int main(int argc, char *argv[])
{
    Tcl_Main(argc, argv, Compute_Init);
}

/*
int Test_AppInit(Tcl_Interp *interp)
{
    if (Tcl_Init(interp) == TCL_ERROR)
    {
        return TCL_ERROR;
    }
#ifdef TCL_XT_TEST
    if (Tclxttest_Init(interp) == TCL_ERROR)
    {
        return TCL_ERROR;
    }
#endif

#ifdef TCL_TEST
    if (Tcltest_Init(interp) == TCL_ERROR)
    {
        return TCL_ERROR;
    }
    Tcl_StaticPackage(interp, "Tcltest", Tcltest_Init, Tcltest_SafeInit);
#endif
}
*/