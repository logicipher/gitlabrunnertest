#include "tcl_point.h"
#include "point.h"

float triangle_circle(const Point &p1, const Point &p2, const Point p3)
{
    float edge[3];
    edge[0] = p1.distance(p2);
    edge[1] = p2.distance(p3);
    edge[2] = p3.distance(p1);
    return edge[0] + edge[1] + edge[2];
}

Point vector_add(const Point &base, const Point &p1, const Point &p2)
{
    Point v1 = Point(p1.x - base.x, p1.y - base.y);
    Point v2 = Point(p2.x - base.x, p2.y - base.y);
    return v1 + v2;
}