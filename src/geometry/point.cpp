#include "point.h"
#include "compute.h"

Point::Point(float x, float y)
{
    this->x = x;
    this->y = y;
}

Point::~Point()
{
    ;
}

float Point::distance(const Point &another) const
{
    float x2, y2;
    x2 = (this->x - another.x) * (this->x - another.x);
    y2 = (this->y - another.y) * (this->y - another.y);
    return quick_sqrt(x2 + y2);
}

Point Point::operator+(const Point &another) const
{
    return Point(this->x + another.x, this->y + another.y);
}
