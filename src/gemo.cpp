#include <iostream>
#include <cmath>
#include <stdlib.h>
#include <ctime>
#include "compute.h"

bool is_equal(double x, double y)
{
    double eps = 1.0e-3;
    return abs(x - y) <= eps;
}

int main()
{
    const int N = 10;
    float test_x[N];
    int test_p[N];
    srand(time(0));
    for (int i = 0; i < N; i++)
    {
        test_x[i] = rand();
        test_p[i] = 2 * i + 1;
    }
    std::cout << "Pow testing start..." << std::endl;
    for (int i = 0; i < N; i++)
    {
        if (is_equal(quick_pow(test_x[i], test_p[i]), pow(test_x[i], test_x[i])))
        {
            std::cout << "quick_pow works wrong!";
            return 0;
        }
    }
    std::cout << "quick_pow works fine." << std::endl;
    std::cout << "Sqrt testing start..." << std::endl;
    for (int i = 0; i < N; i++)
    {
        if (is_equal(quick_sqrt(test_x[i]), sqrt(test_x[i])))
        {
            std::cout << "quick_sqrt works wrong!" << std::endl;
            return 0;
        }
        std::cout << "sqrt of " << test_x[i] << " is " << quick_sqrt(test_x[i]) << std::endl;
    }
    std::cout << "quick_sqrt works fine" << std::endl;
    return 0;
}
