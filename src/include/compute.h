#ifndef __COMPUTE_H_
#define __COMPUTE_H_

const int FLOAT_SIZE = sizeof(float);
const int LONG_SIZE = sizeof(long);
// const int LL_SIZE = sizeof(long long);



#if FLOAT_SIZE == LONG_SIZE
    typedef long LONG;
#else
    typedef int LONG;
#endif


double quick_pow(double x, int p);
float quick_sqrt(float x);


#endif
