#ifndef __POINT_H_
#define __POINT_H_
class Point
{
public:
    float x,y;
    Point(float x, float y);
    float distance(const Point &another) const;
    Point operator+(const Point& another) const;
    ~Point();
};
#endif