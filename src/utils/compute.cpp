#include "compute.h"

double quick_pow(double x, int p)
{
    double res = 1.0;
    while (p)
    {
        if (p & 1)
        {
            res = res * x;
        }
        x = x * x;
        p = p >> 1;
    }
    return res;
}

float quick_sqrt(float x)
{
    LONG i;
    float x2, y;
    const float threehalves = 1.5;

    x2 = x * 0.5;
    y = x;
    i = *(LONG *)&y;
    i = 0x5f375a86 - (i >> 1);
    y = *(float *)&i;
    y = y * (threehalves - (x2 * y * y));
    y = y * (threehalves - (x2 * y * y));
    y = y * (threehalves - (x2 * y * y));
    return x * y;
}
