proc random {min max} {
    set r [expr {rand()}]
    set res [expr {$r * ($max - $min) + $min}]
    return $res
}

set xs [list]
set ys [list]
for {set i 0} {$i < 3} {incr i} {
    lappend xs [random 5 10]
    lappend ys [random 5 10]
}
Point a [lindex $xs 0] [lindex $ys 0]
Point b [lindex $xs 1] [lindex $ys 1]
Point c [lindex $xs 2] [lindex $ys 2]
set len_ab [a distance b]
set len_bc [b distance c]
set len_ca [c distance a]
puts "Three edges' lengths are $len_ab,$len_bc and $len_ca."
puts "The  perimeter of the triangle is [triangle_circle a b c]."
