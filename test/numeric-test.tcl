set test_list [list 5 7 12 23 34]

proc is_equal {x y} {
    if { [expr {abs($x-$y)}] <= 1E-3} {
        return true
    }
    return false
}

foreach x $test_list {
    if {![is_equal [expr {pow($x,7)}]  [quick_pow $x 7]]} {
        puts "quick_pow works wrong at $x ** 7!"
        exit 1
    }
    if {![is_equal [expr {pow($x,19)}] [quick_pow $x 19]]} {
        puts "quick_pow works wrong at $x ** 19!"
        exit 1
    }
    if {![is_equal [expr {sqrt($x)}] [quick_sqrt $x]]} {
        puts "quick_sqrt works wrong at $x!"
        exit 2
    }
}
puts "All works fine."
