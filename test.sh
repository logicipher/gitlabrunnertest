#!/bin/bash

TEST_DIR=$(dirname $0)/test
APP=$TEST_DIR/../build/src/swig/testtcl

i=0
for file in `ls $TEST_DIR`; do
    echo "[$i] Testing script $file..."
    i=$[i+1]
    $APP $TEST_DIR/$file
done